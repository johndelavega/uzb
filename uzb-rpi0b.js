/*
 * OpenZWave test program with firebase.com stuff

   zwave nodejs firebase -  znf

   05:15 AM 06/10/2016 = "005b" node-firebase-uzb.js [from node-firebase.js version 005a] [short name uzb005b.js]

   deployed in RPI2
   	/home/pi/john/node-openzwave/npm1/node_modules/openzwave-shared
	replaced user1 to zwave2   

   10:50 AM 06/23/2017 = "005g"  derived from aeotec 005f, changed to hub_uzb/dimmer6
    7:00 PM 06/22/2017 = "005f"  added firebase.database().ref('zwave7/user1/commands');  add_node remove_node
    4:57 PM 06/21/2017 = "005e"  added win32 os platform & tested ok windows 10 machine
    8:26 PM 02/04/2017 = "005d"  Aeotec /user1/dimmer3 fb3-v005d.aeotec.js
    8:26 PM 01/12/2017 = "005c"  upgraded to firebase 3.x from 2.x
   10:59 PM 05/04/2016 = "005"
   10:38 PM 05/02/2016 = "003f"

   primary archive location:
     fb3-v005g-uzb.js
     https://drive.google.com/drive/folders/0B5xBzgjGUoqJVWxqUEdSTU5MbU0


 */

var m_version = "005g";

console.log("\n fb3 UZB /hub_uzb/dimmer6 version " + m_version + "  -  working folder: " + __dirname);



/*
// os.arch() os.platform() os.type() os.hostname()

var os = require("os");

console.log("\n os.arch()     : " + os.arch() 
	      + "\n os.platform() : " + os.platform()
	      + "\n os.type()     : " + os.type()
	      + "\n os.hostname() : " + os.hostname() + "\n" )
*/



var M_ZNF_TIMEOUT = 1000; // milliseconds


var m_bUpdating = false;

//var m_bDeviceChanging = false;  // false means client app made the change
//var m_bUserChanging   = false;


//var m_bDeviceChanged = true;  
var m_bUserChanged   = true; // true means client app made the change

var firebase = require('./node_modules/firebase');

var config = {
    apiKey: "AIzaSyDWKK5el0c24iRtCPiBV3iEeamH-4whQOM",
    authDomain: "zimplehome.firebaseapp.com",
    databaseURL: "https://zimplehome.firebaseio.com",
    storageBucket: "zimplehome.appspot.com",
    messagingSenderId: "334773009124"
    };
    firebase.initializeApp(config);



//var Firebase = require("firebase");

//var ref = new Firebase("https://dazzling-heat-9040.firebaseio.com/user1/door2");
// m_firebaseRef 

//var fb_ref = new Firebase("https://dazzling-heat-9040.firebaseio.com/zwave2/dimmer3");
var   fb_ref = firebase.database().ref('hub_uzb/dimmer6');  // egd1



var m_bScanCompleted = false;


//var m_updating_user   = false;
//var m_updating_device = false;




//=====================================================================

// if value change by the user from a client app, web ios android
//
fb_ref.on('value', function(dimmer_brightness ) {

 // m_updating_user = true;

  if (m_bUpdating == false) {
	  m_bUpdating = true;
//	  m_bDeviceChanged = false;
	  m_bUserChanged = true;
	  console.log('\n\n dbg user')
  }

console.log('fb_ref.on(`value` - firebase value changed!');
console.log(' m_bUserChanged = ' + m_bUserChanged);
  
  
  	
	
  var brightness1 = dimmer_brightness.val().brightness;
    console.log('brightness1 = ' + brightness1);
	
  brightness1 = parseInt(brightness1.toString());

  if ( m_bScanCompleted && ( brightness1 >= 0 && brightness1 <= 100) ) {

	//m_bDeviceChanged = false; // false means client app made the change
    
	
	if (m_bUserChanged) {
  	console.log('if (m_bUserChanged == true');
		// set dimmer node m_nodeId
		zwave.setValue(m_nodeId,38,1,0,brightness1);
		console.log('fb_ref.on(value,... / zwave.setValue(...');
		
	}
  
	//m_bUserChanged = true;
	
  }

  console.log('\n firebase log - UZB /hub_uzb/dimmer6 - brightness = ' + brightness1);


// wait while device is updating
// wait is limited by reasonable time, ~ twice the reaction time for device  


setTimeout(function(){
	// defer until zwave.on('value changed' ... executes
	m_bUpdating = false;  // reset	
}, M_ZNF_TIMEOUT); // 1 second


/*
  if (m_updating == 1 && m_updating_device == false) { // user
	  m_updating = 0;  // reset
  }
*/
 // m_updating_user = false;

}); // fb_ref.on('value',

//=====================================================================


var fb_zwave7_ref = firebase.database().ref('zwave7/user1/commands');

fb_zwave7_ref.on('value', function( commands ) {
  var s1 = commands.val().switch1;
  var an = commands.val().add_node;
  var rn = commands.val().remove_node;

  console.log('\n zwave7/user1/commands   .val().switch1 = ' + s1 + ', add node = ' + an + ', remove node = ' + rn);

});


//-------


var fb_zwave7_remove_node_ref = firebase.database().ref('zwave7/user1/commands/remove_node');

fb_zwave7_remove_node_ref.on('value', function( remove_node ) {


	console.log('\n\n ZWAVE');

	console.log(' getPollInterval()    = ' + zwave.getPollInterval());

	// Get the version of the Z-Wave API library used by a controller.
	console.log(' getLibraryVersion()  = ' + zwave.getLibraryVersion());

	// Get a string containing the Z-Wave API library type used by a controller
	console.log(' getLibraryTypeName() = ' + zwave.getLibraryTypeName());


	console.log('\n zwave7/user1/commands/remove_node = ' + remove_node.val() );

	console.log('\n\n');

});

//=====================================================================


console.log('\n --------- before firebase code -------- \n' + __dirname);



//=====================================================================
//=====================================================================

// egd1
/* linux */   // var OpenZWave = require('./node_modules/openzwave-shared/lib/openzwave-shared.js');
/* windows */    var OpenZWave = require('./lib/openzwave-shared.js');

var os = require('os');

var zwave = new OpenZWave({
	ConsoleOutput: false,
	Logging: false,
	SaveConfiguration: false,
	DriverMaxAttempts: 3,
	PollInterval: 500,
	SuppressValueRefresh: true,
});


var nodes = [];

var m_nodeId = 6; //egd1  // node3: notification(2): Notification - NoOperation

//=====================================================================

zwave.on('connected', function(homeid) {
	console.log('=================== CONNECTED! ====================');
});

//=====================================================================

zwave.on('driver ready', function(homeid) {
	console.log('=================== DRIVER READY! ====================');
	console.log('scanning homeid=0x%s...', homeid.toString(16));
});

//=====================================================================

zwave.on('driver failed', function() {
	console.log('failed to start driver');

// egd1
console.log(' do not disconnect and exit zwave to test for firebase code :)');

	//zwave.disconnect();

//don't exit to test firebase code :)
	//process.exit();

}); // zwave.on('driver failed'


//=====================================================================

zwave.on('node added', function(nodeid) {
	console.log('=================== NODE ADDED! ====================');
	nodes[nodeid] = {
		manufacturer: '',
		manufacturerid: '',
		product: '',
		producttype: '',
		productid: '',
		type: '',
		name: '',
		loc: '',
		classes: {},
		ready: false,
	};
});

//=====================================================================

zwave.on('value added', function(nodeid, comclass, value) {
	console.log('=================== VALUE ADDED! ====================');
	if (!nodes[nodeid]['classes'][comclass])
		nodes[nodeid]['classes'][comclass] = {};
	nodes[nodeid]['classes'][comclass][value.index] = value;
});





//=====================================================================

// if value changed by device from device button, physical interfaces
// or device sensor changes
zwave.on('value changed', function(nodeid, comclass, value) {

 // m_updating_device = true;

  if (m_bUpdating == false) {
	  m_bUpdating = true;
// 	  m_bDeviceChanged = true;
	  m_bUserChanged = false;
	  console.log('\n\n dbg device')
  }

console.log('zwave.on(`value changed` =================== VALUE CHANGED! ====================');
console.log(' m_bUserChanged = ' + m_bUserChanged);



	if (nodes[nodeid]['ready']) {
		console.log('dbg1-node%d: changed: %d:%s:%s->%s', nodeid, comclass,
			    value['label'],
			    nodes[nodeid]['classes'][comclass][value.index]['value'],
			    value['value']);
	}

	nodes[nodeid]['classes'][comclass][value.index] = value;

//	m_bUserChanged = false;

 // if (!m_bUserChanged) 
//	if (m_bDeviceChanged  && ( nodeid == 3 && comclass == 38) ) { // filter node 3 only specific testing debugging
 // if device changed 
	if (!m_bUserChanged  && ( nodeid == m_nodeId && comclass == 38) ) { // filter node 3 only specific testing debugging
 	  console.log('if (m_bDeviceChanged == true  - value[`value`] = ' + value['value']);	
		fb_ref.set( { brightness: value['value'] });
		console.log('zwave.on(value changed,... / fb_ref.set( { brightness: value[`value`]  });');
//		m_bDeviceChanged = false;
	}

//	m_bDeviceChanged = true;

setTimeout(function(){
	m_bUpdating = false;  // reset	
}, M_ZNF_TIMEOUT); // 1 second




/*
  if (m_updating == 2 && m_updating_user == false) { // device
	  m_updating = 0;  // reset
  }
*/
//  m_updating_device = false;

}); // zwave.on('value changed',


//=====================================================================

zwave.on('value removed', function(nodeid, comclass, index) {
	console.log('=================== VALUE REMOVED! ====================');
	if (nodes[nodeid]['classes'][comclass] &&
	    nodes[nodeid]['classes'][comclass][index])
		delete nodes[nodeid]['classes'][comclass][index];
});

//=====================================================================

zwave.on('node ready', function(nodeid, nodeinfo) {
	console.log('=================== NODE READY! ====================');
	nodes[nodeid]['manufacturer'] = nodeinfo.manufacturer;
	nodes[nodeid]['manufacturerid'] = nodeinfo.manufacturerid;
	nodes[nodeid]['product'] = nodeinfo.product;
	nodes[nodeid]['producttype'] = nodeinfo.producttype;
	nodes[nodeid]['productid'] = nodeinfo.productid;
	nodes[nodeid]['type'] = nodeinfo.type;
	nodes[nodeid]['name'] = nodeinfo.name;
	nodes[nodeid]['loc'] = nodeinfo.loc;
	nodes[nodeid]['ready'] = true;
	console.log('node%d: %s, %s', nodeid,
		    nodeinfo.manufacturer ? nodeinfo.manufacturer
					  : 'id=' + nodeinfo.manufacturerid,
		    nodeinfo.product ? nodeinfo.product
				     : 'product=' + nodeinfo.productid +
				       ', type=' + nodeinfo.producttype);
	console.log('node%d: name="%s", type="%s", location="%s"', nodeid,
		    nodeinfo.name,
		    nodeinfo.type,
		    nodeinfo.loc);
	for (comclass in nodes[nodeid]['classes']) {
		switch (comclass) {
		case 0x25: // COMMAND_CLASS_SWITCH_BINARY
		case 0x26: // COMMAND_CLASS_SWITCH_MULTILEVEL
			zwave.enablePoll(nodeid, comclass);
			break;
		}
		var values = nodes[nodeid]['classes'][comclass];
		console.log('node%d: class %d', nodeid, comclass);
		for (idx in values)
			console.log('node%d:   %s=%s', nodeid, values[idx]['label'], values[idx]['value']);
	}

}); // zwave.on('node ready'

//=====================================================================

zwave.on('notification', function(nodeid, notif, help) {
	console.log('=================== NOTIFICATION! ====================');
	console.log('node%d: notification(%d): %s', nodeid, notif, help);
});



zwave.on('scan complete', function() {
	console.log('=================== SCAN COMPLETE! ====================');
        m_bScanCompleted = true;
	console.log('scan complete, hit ^C to finish.');
});



zwavedriverpaths = {
	"darwin" : '/dev/cu.usbmodem1411',
	"linux"  : '/dev/ttyACM0', // aeotec / z-wave.me uzb usb stick
//	"windows": '\\\\.\\COM3',
	"win32"  : '\\\\.\\COM4'
}


//=====================================================================

console.log ('os platform: ' + os.platform() );

console.log("connecting to " + zwavedriverpaths[os.platform()]);
zwave.connect( zwavedriverpaths[os.platform()] );

//=====================================================================

process.on('SIGINT', function() {
	console.log('disconnecting...');
	zwave.disconnect();
	process.exit();
});

//=====================================================================
